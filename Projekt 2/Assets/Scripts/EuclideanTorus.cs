﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EuclideanTorus : MonoBehaviour {
    
	// Update is called once per frame
	void Update () {

        if (transform.position.x > 9)
        {

            transform.position = new Vector2(-9, transform.position.y);

        }
        else if (transform.position.x < -9)
        {
            transform.position = new Vector2(9, transform.position.y);
        }

        else if (transform.position.y > 6)
        {
            transform.position = new Vector2(transform.position.x, -6);
        }

        else if (transform.position.y < -6)
        {
            transform.position = new Vector2(transform.position.x, 6);
        }
    }
}
