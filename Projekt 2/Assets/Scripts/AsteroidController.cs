﻿using UnityEngine;
using System.Collections;

public class AsteroidController : MonoBehaviour
{

    public AudioClip destroy;
    public GameObject smallAsteroid;
    private Rigidbody2D rb2d;

    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddForce(transform.up * Random.Range(-50.0f, 150.0f));
        rb2d.angularVelocity = Random.Range(-0.0f, 90.0f);
    }

    void OnCollisionEnter2D(Collision2D c)
    {

        if (c.gameObject.tag.Equals("Bullet"))
        {
            Destroy(c.gameObject);
            
            if (tag.Equals("Large Asteroid"))
            {
                Instantiate(smallAsteroid,
                    new Vector3(transform.position.x - 1f,
                        transform.position.y - 1f, 0),
                        Quaternion.Euler(0, 0, 90));
                
                Instantiate(smallAsteroid,
                    new Vector3(transform.position.x + 0f,
                        transform.position.y + 0f, 0),
                        Quaternion.Euler(0, 0, 0));
                
                Instantiate(smallAsteroid,
                    new Vector3(transform.position.x + 1f,
                        transform.position.y - 1f, 0),
                        Quaternion.Euler(0, 0, 270));

                GameController.instance.SplitAsteroid();

            }
            else
            {
                GameController.instance.DecrementAsteroids();
            }

            AudioSource.PlayClipAtPoint(destroy, Camera.main.transform.position);
            
            GameController.instance.ShipScored();
            
            Destroy(gameObject);

        }

    }
}