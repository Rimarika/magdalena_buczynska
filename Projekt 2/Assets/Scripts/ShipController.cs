﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShipController : MonoBehaviour
{
    float rotationSpeed = 60.0f;
    float thrustForce = 100f;

    public AudioClip crash;
    public AudioClip shoot;

    public GameObject bullet;

    private Rigidbody2D rb2d;

    private bool isDead = false;
    public int life = 3;
    public Text livesText;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void Update()
    {
        if(isDead == false)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb2d.AddForce(transform.up * thrustForce * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                rb2d.AddForce(transform.up * -thrustForce * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Rotate(0, 0, -rotationSpeed * Time.deltaTime);
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ShootBullet();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.tag != "Bullet")
        {

            AudioSource.PlayClipAtPoint(crash, Camera.main.transform.position);
            
            transform.position = new Vector3(0, 0, 0);
            
            rb2d.velocity = new Vector3(0, 0, 0);

            if (life >= 1)
            {
                life -= 1;
            }
            switch (life)
            {
                case 3:
                    livesText.text = "LIVES: " + life;
                    break;
                case 2:
                    livesText.text = "LIVES: " + life;
                    break;
                case 1:
                    livesText.text = "LIVES: " + life;
                    break;
                case 0:
                    livesText.text = "LIVES: " + life;
                    isDead = true;
                    GameController.instance.ShipDied();
                    break;
            }
        }
    }

    void ShootBullet()
    {
        Instantiate(bullet,new Vector3(transform.position.x, transform.position.y, 0), transform.rotation);
        
        AudioSource.PlayClipAtPoint(shoot, Camera.main.transform.position);
    }
}