﻿using UnityEngine;
using System.Collections;

public class Column : MonoBehaviour 
{
    public bool isMoving = true;
    public float speed = 0.02f;

    public void Update()
    {
        if (isMoving)
        {
            if (GameControl.instance.gameOver == false)
            {
                if ((transform.position.y < 3.5f) && (transform.position.y > -1f))
                {
                    this.transform.Translate(new Vector2(0, speed));
                }
                else
                {
                    speed *= -1;
                    this.transform.Translate(new Vector2(0, speed));
                }
            }
        }
        
    }

    void OnTriggerEnter2D(Collider2D other)
	{
		if(other.GetComponent<Bird>() != null)
		{
			//If the bird hits the trigger collider in between the columns then
			//tell the game control that the bird scored.
			GameControl.instance.BirdScored();
		}
	}
}
