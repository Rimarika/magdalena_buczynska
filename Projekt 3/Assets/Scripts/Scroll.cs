﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroll : MonoBehaviour {

    public Transform scroll;
    private RaycastHit hit;
    public AudioClip takeScroll;

    void Awake()
    {
        Physics.Raycast(scroll.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }

    void Start()
    {
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
    }
    
    void Update()
    {
        transform.Rotate(new Vector3(0, 0.5f, 0));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            PlayerStats.instance.haveScroll = true;
            SpawnEnemy.instance.Spawn();
            AudioSource.PlayClipAtPoint(takeScroll, Camera.main.transform.position);
            Destroy(gameObject);
        }
    }
}
