﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{

    public Texture2D crosshairTexture;
    public AudioClip pistolShot;
    public AudioClip reloadSound;
    public int maxAmmo = 300;
    public int clipSize = 20;
    public Text ammoText;
    private int currentAmmo = 40;
    private int currentClip;

    public float reloadTime = 2.0f;
    private bool isReloading = false;

    private float timer = 0.0f;

    public GameObject bulletHole;
    public GameObject bloodParticles;
    public float damage = 5.0f;

    private Rect position;
    private float range = 100.0f;
    private GameObject pistolSparks;
    private Vector3 fwd;
    private RaycastHit hit;
    private float zoomFieldOfView = 30.0f;
    private float defaultFieldOfView = 60.0f;
    

    // Use this for initialization
    void Start()
    {
        Cursor.visible = false;
        position = new Rect((Screen.width - crosshairTexture.width) / 2,
                            (Screen.height - crosshairTexture.height) / 2,
                            crosshairTexture.width,
                            crosshairTexture.height);

        pistolSparks = GameObject.Find("Sparks");
        ParticleSystem ps = pistolSparks.GetComponent<ParticleSystem>();
        var em = ps.emission;
        em.enabled = false;

        currentClip = clipSize;
    }

    // Update is called once per frame
    void Update()
    {
        Transform tf = transform.parent.GetComponent<Transform>();
        fwd = tf.TransformDirection(Vector3.forward);


            if (((Input.GetButtonDown("Fire1") && currentClip == 0) || Input.GetButtonDown("Reload")) && currentClip < clipSize)
        {
            if (currentAmmo > 0)
            {
                AudioSource.PlayClipAtPoint(reloadSound, Camera.main.transform.position);
                isReloading = true;
            }
        }

        if (isReloading)
        {
            timer += Time.deltaTime;
            if (timer >= reloadTime)
            {
                int needAmmo = clipSize - currentClip;

                if (currentAmmo >= needAmmo)
                {
                    currentClip = clipSize;
                    currentAmmo -= needAmmo;
                }
                else
                {
                    currentClip += currentAmmo;
                    currentAmmo = 0;
                }
                
                isReloading = false;
                timer = 0.0f;
            }
        }

        if (currentClip > 0 && !isReloading)
        {
            if (Input.GetButtonDown("Fire1") && GameControl.instance.gameOver == false && GameControl.instance.win == false)
            {
                if(Time.timeScale == 1)
                {
                    currentClip--;
                    ParticleSystem ps = pistolSparks.GetComponent<ParticleSystem>();
                    var em = ps.emission;
                    em.enabled = true;
                    StartCoroutine("waitAndChangeEmission");

                    AudioSource.PlayClipAtPoint(pistolShot, Camera.main.transform.position);
                }
                

                if (Physics.Raycast(tf.position, fwd, out hit))
                {
                    if (hit.collider.tag.Equals("Head") && hit.distance < range)
                    {
                        hit.collider.gameObject.GetComponentInParent<EnemyAI>().hp = 0;
                        hit.transform.gameObject.SendMessage("takeHit", damage);
                        GameControl.instance.PlayerScored(5);

                        if (hit.collider.gameObject.GetComponentInParent<EnemyAI>().hp >= 0)
                        {
                            GameObject go;
                            go = Instantiate(bloodParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
                            Destroy(go, 0.3f);
                        }

                    }

                    else if (hit.transform.tag.Equals("Enemy") && hit.distance < range)
                    {
                        hit.transform.gameObject.SendMessage("takeHit", damage);

                        if (hit.collider.gameObject.GetComponentInParent<EnemyAI>().hp >= 0)
                        {
                            GameControl.instance.PlayerScored(1);
                            GameObject go;
                            go = Instantiate(bloodParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
                            Destroy(go, 0.3f);
                        }
                    }

                    else if (hit.collider.tag.Equals("HeadBoss") && hit.distance < range)
                    {
                        hit.transform.gameObject.SendMessage("takeHit", 15);
                        GameControl.instance.PlayerScored(10);

                        if (hit.collider.gameObject.GetComponentInParent<EnemyAI_Boss>().hp >= 0)
                        {
                            GameObject go;
                            go = Instantiate(bloodParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
                            Destroy(go, 0.3f);
                        }
                    }

                    else if (hit.transform.tag.Equals("EnemyBoss") && hit.distance < range)
                    {
                        hit.transform.gameObject.SendMessage("takeHit", damage);

                        if (hit.collider.gameObject.GetComponentInParent<EnemyAI_Boss>().hp >= 0)
                        {
                            GameControl.instance.PlayerScored(1);
                            GameObject go;
                            go = Instantiate(bloodParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
                            Destroy(go, 0.3f);
                        }
                    }

                    else if (hit.distance < range)
                    {
                        if (hit.distance > 1)
                        {
                            GameObject go;
                            go = Instantiate(bulletHole, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal)) as GameObject;
                            Destroy(go, 5);
                        }
                    }
                }
            }
        }
        
        if (gameObject.GetComponentInParent<Camera>() is Camera)
        {
            Camera cam = gameObject.GetComponentInParent<Camera>();
            if (Input.GetButton("Fire2"))
            {
                if (cam.fieldOfView > zoomFieldOfView)
                {
                    cam.fieldOfView--;
                }
            }
            else
            {
                if (cam.fieldOfView < defaultFieldOfView)
                {
                    cam.fieldOfView++;
                }
            }
        }
    }

    IEnumerator waitAndChangeEmission()
    {
        yield return new WaitForSeconds(0.2f);
        ParticleSystem ps = pistolSparks.GetComponent<ParticleSystem>();
        var em = ps.emission;
        em.enabled = false;
    }

    void OnGUI()
    {
        if (Time.timeScale == 1)
        {
            GUI.DrawTexture(position, crosshairTexture);
        }
        ammoText.text = "ammo:  " + currentClip + " / " + currentAmmo;
    }

    public bool canGetAmmo()
    {
        if (currentAmmo == maxAmmo)
        {
            return false;
        }
        return true;
    }

    void addAmmo(int data)
    {
        int ammoToAdd = data;

        if (maxAmmo - currentAmmo >= ammoToAdd)
        {
            currentAmmo += ammoToAdd;
        }
        else
        {
            currentAmmo = maxAmmo;
        }
    }
}
