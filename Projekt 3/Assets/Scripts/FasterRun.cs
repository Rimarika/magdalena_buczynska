﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FasterRun : MonoBehaviour {
    
    public bool faster = false;
    public Transform potion;
    private RaycastHit hit;
    BoxCollider m_Collider;
    private float currentStamina;

    public GameObject fasterRun;

    void Awake()
    {
        GameObject potionB = GameObject.Find("POTION B");
        GameObject image = Instantiate(fasterRun);
        image.transform.SetParent(potionB.transform, false);
        fasterRun = image;

        Physics.Raycast(potion.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }

    void Start()
    {
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
        m_Collider = GetComponent<BoxCollider>();
    }
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0.5f, 0));


        if (faster)
        {
            PlayerStats.instance.currentStamina = currentStamina;
            StartCoroutine(waitAndChangeRunSpeed());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            faster = true;
            PlayerStats.instance.Faster();
            m_Collider.enabled = false;
            fasterRun.SetActive(true);

            currentStamina = PlayerStats.instance.currentStamina;
        }
    }

    IEnumerator waitAndChangeRunSpeed()
    {
        yield return new WaitForSeconds(10f);
        PlayerStats.instance.ChangeFaster();
        fasterRun.SetActive(false);
        Destroy(gameObject);
    }
}
