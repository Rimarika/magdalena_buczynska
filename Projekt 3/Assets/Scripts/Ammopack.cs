﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammopack : MonoBehaviour {

    public int ammunition;
    public Transform ammo;
    private RaycastHit hit;
    public GameObject ammoBonus;
    public Text ammoText;
    public bool ammoB = false;
    BoxCollider m_Collider;

    void Awake()
    {
        GameObject canvas = GameObject.Find("Canvas");
        GameObject text = Instantiate(ammoBonus);
        text.transform.SetParent(canvas.transform, false);
        ammoBonus = text;

        Physics.Raycast(ammo.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }

    void Start()
    {
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
        m_Collider = GetComponent<BoxCollider>();
    }
    // Update is called once per frame
    void Update () {
        transform.Rotate(new Vector3(0, 0, 1));

        if (ammoB)
        {
            StartCoroutine("waitAndChangeAmmo");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            ammunition = Random.Range(5, 50);

            Transform gun = other.transform.Find("FirstPersonCharacter/gun");
            if (gun.GetComponent<Shooting>().canGetAmmo())
            {
                gun.SendMessage("addAmmo", ammunition);
            }

            ammoText.text = "+" + ammunition;
            ammoBonus.GetComponent<Text>().text = ammoText.text;
            ammoBonus.SetActive(true);
            ammoB = true;
            m_Collider.enabled = false;
        }
    }

    IEnumerator waitAndChangeAmmo()
    {
        yield return new WaitForSeconds(0.7f);
        ammoBonus.SetActive(false);
        Destroy(gameObject);
    }
}
