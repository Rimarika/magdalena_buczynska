﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest : MonoBehaviour
{
    public int bonus;

    public Transform chest;
    private RaycastHit hit;
    public bool bonusScore = false;
    public GameObject scoreBonus;
    public Text scoreText;
    public AudioClip collectCoin;
    BoxCollider m_Collider;

    void Awake()
    {
        GameObject canvas = GameObject.Find("Canvas");
        GameObject text = Instantiate(scoreBonus);
        text.transform.SetParent(canvas.transform, false);
        scoreBonus = text;
        Physics.Raycast(chest.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }

    void Start()
    {
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
        m_Collider = GetComponent<BoxCollider>();
    }
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, 1));

        if (bonusScore)
        {
            StartCoroutine("waitAndChangeScore");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            bonus = Random.Range(1, 100);
            scoreText.text = "+" + bonus;
            scoreBonus.GetComponent<Text>().text = scoreText.text;
            scoreBonus.SetActive(true);
            AudioSource.PlayClipAtPoint(collectCoin, Camera.main.transform.position);
            bonusScore = true;
            m_Collider.enabled = false;
        }
    }

    IEnumerator waitAndChangeScore()
    {
        yield return new WaitForSeconds(0.7f);
        GameControl.instance.PlayerScored(bonus);

        scoreBonus.SetActive(false);
        Destroy(gameObject);
    }
}
