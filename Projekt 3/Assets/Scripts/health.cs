﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public Transform health;
    private RaycastHit hit;

    void Awake()
    {
        Physics.Raycast(health.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }
    // Use this for initialization
    void Start () {
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
    }
	
	// Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, 1));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            PlayerStats.instance.currentHealth = PlayerStats.instance.maxHealth;
            Destroy(gameObject);
        }
    }
}
