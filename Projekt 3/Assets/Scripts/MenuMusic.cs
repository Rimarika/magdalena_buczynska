﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuMusic : MonoBehaviour {

    void Awake()
    {
        Object.DontDestroyOnLoad(gameObject);
        SceneManager.LoadScene("Menu");
    }
}
