﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishGame : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && PlayerStats.instance.haveScroll)
        {
            GameControl.instance.PlayerWin();
        }
    }
}
