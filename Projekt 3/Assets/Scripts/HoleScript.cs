﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleScript : MonoBehaviour {

    public Collider player;
    public TerrainCollider tCollider;
    

    void OnTriggerEnter(Collider c)
    {
        if (c.tag == "Player")
        {
            Physics.IgnoreCollision(player, tCollider, true);
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (c.tag == "Player")
        {
            Physics.IgnoreCollision(player, tCollider, false);
        }
    }
}
