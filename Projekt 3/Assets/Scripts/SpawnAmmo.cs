﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAmmo : MonoBehaviour {

    public GameObject respawnObject;
    private Vector3 minLocation = new Vector3(150, 150, 70);
    private Vector3 maxLocation = new Vector3(600, 150, 660);

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 60; i++)
        {
            Vector3 objectPos = new Vector3(0, 0, 0);

            for (int j = 0; j < 10; j++)
            {
                objectPos = new Vector3(Random.Range(minLocation.x, maxLocation.x),
                                             Random.Range(minLocation.y, maxLocation.y),
                                             Random.Range(minLocation.z, maxLocation.z));

                Collider[] hitColliders = Physics.OverlapSphere(objectPos, 0.5f);

                if (hitColliders.Length == 0)
                {
                    break;
                }
            }
            Instantiate(respawnObject, objectPos, Quaternion.Euler(-90, 0, 0));
        }
    }
}
