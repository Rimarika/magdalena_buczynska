﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI_Boss : MonoBehaviour
{
    public float rotationSpeed = 6.0f;
    public bool smoothRotation = true;

    public float walkSpeed = 3f;
    public float range = 10f;
    public float attackDistance = 3f;

    private Transform enemy;
    private Transform player;
    private bool seeThePlayer = false;
    private Vector3 positionPlayerXYZ;

    public float attackDemage = 10.0f;
    public float attackDelay = 1.0f;
    public float hp = 60.0f;
    public Transform[] transforms;

    private float timer = 0;
    private Animator animator;

    public bool hitPlayer = false;
    public bool hitP = false;
    private float opacity = 0.0f;

    public Transform feet;
    private RaycastHit hit;

    void OnGUI()
    {
        if (hitPlayer)
        {
            hitP = true;
            opacity = 1.0f;
        }

        if (hitP)
        {
            GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, opacity);
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), PlayerStats.instance.bloodTexture, ScaleMode.ScaleAndCrop);
            StartCoroutine("waitAndChangeOpacity");
        }

        if (opacity <= 0)
        {
            hitP = false;
        }
    }

    IEnumerator waitAndChangeOpacity()
    {
        yield return new WaitForEndOfFrame();
        opacity -= 0.05f;
    }

    void Awake()
    {
        Physics.Raycast(feet.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }
    // Use this for initialization
    void Start()
    {
        enemy = transform;

        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }

        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (hp > 0)
        {
            player = GameObject.FindWithTag("Player").transform;

            positionPlayerXYZ = new Vector3(player.position.x, enemy.position.y, player.position.z);

            float distance = Vector3.Distance(enemy.position, player.position);

            seeThePlayer = false;
            hitPlayer = false;

            animator.SetBool("isIdle", false);

            if (distance <= range && distance >= attackDistance)
            {
                animator.SetBool("isWalking", true);
                animator.SetBool("isAttacking", false);

                seeThePlayer = true;

                enemy.position = Vector3.MoveTowards(enemy.position, positionPlayerXYZ, walkSpeed * Time.deltaTime);

            }
            else if (distance <= attackDistance)
            {
                seeThePlayer = true;

                if (timer <= 0)
                {
                    animator.SetBool("isAttacking", true);
                    animator.SetBool("isWalking", false);
                    timer = attackDelay;
                    if (PlayerStats.instance.enemyHit)
                    {
                        player.SendMessage("takeHit", attackDemage);
                        hitPlayer = true;
                    }
                }
            }
            else
            {
                animator.SetBool("isIdle", true);
                animator.SetBool("isWalking", false);
                animator.SetBool("isAttacking", false);
                animator.SetBool("isDying", false);
            }

            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }

            lookAtMe();
        }
    }

    void takeHit(float demage)
    {
        StartCoroutine("waitAndChangeOpacity");
        hp -= demage;
        if(hp>=0)
        {
            //GameControl.instance.PlayerScored(1);
        }
        if (hp <= 0)
        {
            animator.SetBool("isDying", true);
            animator.SetBool("isIdle", false);
            animator.SetBool("isWalking", false);
            animator.SetBool("isAttacking", false);
            StartCoroutine("waitDestroy");
        }
    }

    void lookAtMe()
    {
        if (smoothRotation && seeThePlayer == true)
        {
            Quaternion rotation = Quaternion.LookRotation(positionPlayerXYZ - enemy.position);
            enemy.rotation = Quaternion.Slerp(enemy.rotation, rotation, Time.deltaTime * rotationSpeed);
        }
        else if (!smoothRotation && seeThePlayer == true)
        {
            transform.LookAt(positionPlayerXYZ);
        }

    }

    IEnumerator waitDestroy()
    {
        yield return new WaitForSeconds(10f);
        Destroy(gameObject);
    }

}
