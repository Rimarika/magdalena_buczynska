﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {

    public static GameControl instance;
    public Text scoreText;
    public Text highScoreText;
    public Text timerText;
    public GameObject gameOvertext;
    public GameObject wintext;
    public GameObject pauza;

    public int score = 0;
    public int highScore = 0;
    private int minutes = 15;
    private float seconds = 0;

    public bool gameOver = false;
    public bool win = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

     void Start()
    {
        highScore = PlayerPrefs.GetInt("highScore", 0);
        highScoreText.text = "NAJLEPSZY WYNIK: " + highScore;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && (gameOver || win))
        {
            SceneManager.LoadScene("Menu");
        }

        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauza.SetActive(true);
            Cursor.visible = true;
            Time.timeScale = 0;
        }

        if (pauza.activeSelf == true)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
        }

        if (win || gameOver)
            return;

        seconds -= Time.deltaTime;
        timerText.text = minutes + ":" + (int)seconds;

        if (seconds <= 0)
        {
            minutes--;
            seconds = 60;
        }

        if (minutes <= 0 && (int)seconds <= 0)
            PlayerDied();
    }

    public void PlayerScored(int points)
    {
        if (gameOver)
            return;
        score += points;
        scoreText.text = "WYNIK: " + score.ToString();
    }

    public void PlayerDied()
    {
        gameOvertext.SetActive(true);
        gameOver = true;
        Time.timeScale = 0;
        timerText.color = Color.yellow;
    }

    public void PlayerWin()
    {
        wintext.SetActive(true);
        win = true;
        Time.timeScale = 0;
        timerText.color = Color.yellow;
        
        score += (minutes * 60 + (int)seconds) * 2;
        scoreText.text = "WYNIK: " + score.ToString();

        if (score > highScore)
        {
            highScore = score;
            highScoreText.text = "NAJLEPSZY WYNIK: " + highScore;

            PlayerPrefs.SetInt("highScore", highScore);
        }
    }


    public void ChangeYes()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ChangeNo()
    {
        pauza.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1;
    }
}
