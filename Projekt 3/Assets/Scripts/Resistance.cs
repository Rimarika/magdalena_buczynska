﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resistance : MonoBehaviour {

    public bool resistance = false;

    public Transform potion;
    private RaycastHit hit;

    BoxCollider m_Collider;
    public GameObject resistanceToAttacks;

    void Awake()
    {
        GameObject potionD = GameObject.Find("POTION D");
        GameObject image = Instantiate(resistanceToAttacks);
        image.transform.SetParent(potionD.transform, false);
        resistanceToAttacks = image;

        Physics.Raycast(potion.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }

    void Start()
    {
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
        m_Collider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0.5f, 0));

        if (resistance)
        {
            StartCoroutine(waitAndChangeInvisibility());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            resistance = true;
            PlayerStats.instance.enemyHit = false;
            m_Collider.enabled = false;
            resistanceToAttacks.SetActive(true);
        }
    }

    IEnumerator waitAndChangeInvisibility()
    {
        yield return new WaitForSeconds(20f);
        PlayerStats.instance.enemyHit = true;
        resistanceToAttacks.SetActive(false);
        Destroy(gameObject);
    }
}
