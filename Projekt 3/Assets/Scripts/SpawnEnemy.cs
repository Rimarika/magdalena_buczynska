﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{

    public static SpawnEnemy instance;
    public GameObject respawnObject;
    private Vector3 minLocation = new Vector3(150, 150, 280);
    private Vector3 maxLocation = new Vector3(700, 150, 660);

    private Vector3 minLocation2 = new Vector3(150, 150, 70);
    private Vector3 maxLocation2 = new Vector3(540, 150, 280);

    private Vector3 minLocation3 = new Vector3(450, 150, 70);
    private Vector3 maxLocation3 = new Vector3(600, 150, 300);

    void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 70; i++)
        {
            Vector3 objectPos = new Vector3(0, 0, 0);

            for (int j = 0; j < 10; j++)
            {
                objectPos = new Vector3(Random.Range(minLocation.x, maxLocation.x),
                                             Random.Range(minLocation.y, maxLocation.y),
                                             Random.Range(minLocation.z, maxLocation.z));

                Collider[] hitColliders = Physics.OverlapSphere(objectPos, 0.5f);

                if (hitColliders.Length == 0)
                {
                    break;
                }
            }
            Instantiate(respawnObject, objectPos, Quaternion.identity);
        }

        for (int i = 0; i < 40; i++)
        {
            Vector3 objectPos = new Vector3(0, 0, 0);

            for (int j = 0; j < 10; j++)
            {
                objectPos = new Vector3(Random.Range(minLocation2.x, maxLocation2.x),
                                             Random.Range(minLocation2.y, maxLocation2.y),
                                             Random.Range(minLocation2.z, maxLocation2.z));

                Collider[] hitColliders = Physics.OverlapSphere(objectPos, 0.5f);

                if (hitColliders.Length == 0)
                {
                    break;
                }
            }
            Instantiate(respawnObject, objectPos, Quaternion.identity);
        }
    }
    public void Spawn()
    {
        for (int i = 0; i < 60; i++)
        {
            Vector3 objectPos = new Vector3(0, 0, 0);

            for (int j = 0; j < 10; j++)
            {
                objectPos = new Vector3(Random.Range(minLocation3.x, maxLocation3.x),
                                             Random.Range(minLocation3.y, maxLocation3.y),
                                             Random.Range(minLocation3.z, maxLocation3.z));

                Collider[] hitColliders = Physics.OverlapSphere(objectPos, 0.5f);

                if (hitColliders.Length == 0)
                {
                    break;
                }
            }
            Instantiate(respawnObject, objectPos, Quaternion.identity);
        }

    }
}
