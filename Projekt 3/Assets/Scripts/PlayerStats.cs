﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    public float maxHealth = 100;
    public float currentHealth = 100;
    private float maxArmour = 100;
    private float currentArmour = 100;
    private float maxStamina = 100;
    public float currentStamina = 100;

    private float barWidth;
    private float barHeight;

    private float textWidth;
    private float textHeight;

    private float canHeal = 0.0f;
    private float canRegenerate = 0.0f;

    private CharacterController chCont;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsC;
    private Vector3 lastPosition;

    public Text health;
    public Text armour;
    public Text energy;
    public Texture2D healthTexture;
    public Texture2D armourTexture;
    public Texture2D staminaTexture;

    private GUIStyle guiStyle = new GUIStyle();
    
    public Texture2D bloodTexture;

    public static PlayerStats instance;

    public Transform feet;
    private RaycastHit hit;

    public float range = 50f;
    public float attackDistance = 3f;
    public bool enemyHit = true;

    public bool haveScroll = false;

    void Awake()
    {
        instance = this;
        barHeight = Screen.height * 0.02f;
        barWidth = barHeight * 10.0f;
        textHeight = Screen.height * 0.02f;
        textWidth = barHeight * 7.0f;

        chCont = GetComponent<CharacterController>();
        fpsC = gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();

        lastPosition = transform.position;

        Physics.Raycast(feet.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }

    void OnGUI()
    {
        guiStyle.fontSize = 12;
        guiStyle.normal.textColor = Color.white;
        guiStyle.alignment = TextAnchor.UpperRight;

        GUI.DrawTexture(new Rect(Screen.width - barWidth - 10,
                                 Screen.height - barHeight - 10,
                                 currentHealth * barWidth / maxHealth,
                                 barHeight),
                        healthTexture);

        GUI.DrawTexture(new Rect(Screen.width - barWidth - 10,
                                 Screen.height - barHeight * 2 - 20,
                                 currentArmour * barWidth / maxArmour,
                                 barHeight),
                        armourTexture);

        GUI.DrawTexture(new Rect(Screen.width - barWidth - 10,
                                 Screen.height - barHeight * 3 - 30,
                                 currentStamina * barWidth / maxStamina,
                                 barHeight),
                        staminaTexture);

        GUI.Label(new Rect(Screen.width - barWidth - textWidth - 12,
                                 Screen.height - textHeight - 10,
                                 textWidth, textHeight), "życie", guiStyle);

        GUI.Label(new Rect(Screen.width - barWidth - textWidth - 12,
                                 Screen.height - textHeight * 2 - 20,
                                 textWidth, textHeight), "pancerz", guiStyle);

        GUI.Label(new Rect(Screen.width - barWidth - textWidth - 12,
                                 Screen.height - textHeight * 3 - 30,
                                 textWidth, textHeight), "wytrzymałość", guiStyle);
    }

    // Update is called once per frame

    void Update()
    {
        if (canHeal > 0.0f)
        {
            canHeal -= Time.deltaTime;
        }
        if (canRegenerate > 0.0f)
        {
            canRegenerate -= Time.deltaTime;
        }
        if (canHeal <= 0.0f && currentHealth < maxHealth)
        {
            regenerate(ref currentHealth, maxHealth);
        }
        if (canRegenerate <= 0.0f && currentStamina < maxStamina)
        {
            regenerate(ref currentStamina, maxStamina);
        }

        if (currentHealth <= 0)
        {
            GameControl.instance.PlayerDied();
        }

    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift) && lastPosition != transform.position && currentStamina > 0) {

            lastPosition = transform.position;
            currentStamina -= 0.1f;
            currentStamina = Mathf.Clamp(currentStamina, 0, maxStamina);
            canRegenerate = 3.0f;
        }

        if (currentStamina > 0)
        {
            fpsC.CanRun = true;
        }
        else
        {
            fpsC.CanRun = false;
        }
    }

    void takeHit(float damage)
    {
        if (enemyHit)
        {
            if (currentArmour > 0)
            {
                currentArmour -= damage;
                if (currentArmour < 0)
                {
                    currentHealth += currentArmour;
                    currentArmour = 0;
                }
            }
            else
            {
                currentHealth -= damage;
            }

            if (currentHealth < maxHealth)
            {
                canHeal = 3.0f;
            }

            currentArmour = Mathf.Clamp(currentArmour, 0, maxArmour);
            currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        }
    }

    void regenerate(ref float currentStat, float maxStat)
    {
        currentStat += maxStat * 0.005f;
        Mathf.Clamp(currentStat, 0, maxStat);
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Water"))
        {
            takeHit(0.05f);
        }
    }

    public void Faster()
    {
        fpsC.m_RunSpeed = 15;
        fpsC.m_RunstepLenghten = 0.4f;
    }

    public void ChangeFaster()
    {
        fpsC.m_RunSpeed = 10;
        fpsC.m_RunstepLenghten = 0.7f;
    }
}
