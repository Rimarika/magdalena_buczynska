﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject panelScore;

    // Use this for initialization
    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        Time.timeScale = 1;
    }

    public void NewGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void InfoGame()
    {
        panelScore.SetActive(true);
    }

    public void CloseInfoGame()
    {
        panelScore.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
