﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invisibility : MonoBehaviour {

    public bool invisible = false;

    public Transform potion;
    private RaycastHit hit;

    BoxCollider m_Collider;
    public GameObject invisibility;

    void Awake()
    {
        GameObject potionE = GameObject.Find("POTION E");
        GameObject image = Instantiate(invisibility);
        image.transform.SetParent(potionE.transform, false);
        invisibility = image;

        Physics.Raycast(potion.position, new Vector3(0, -1, 0), out hit);
        transform.position -= new Vector3(0, hit.distance, 0);
    }

    void Start()
    {
        if (GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
        m_Collider = GetComponent<BoxCollider>();
    }
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0.5f, 0));

        if (invisible)
        {
            StartCoroutine(waitAndChangeInvisibility());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            PlayerStats.instance.range = 0;
            PlayerStats.instance.attackDistance = 0;
            PlayerStats.instance.enemyHit = false;
            invisible = true;
            m_Collider.enabled = false;
            invisibility.SetActive(true);
        }
    }

    IEnumerator waitAndChangeInvisibility()
    {
        yield return new WaitForSeconds(30f);
        PlayerStats.instance.range = 50f;
        PlayerStats.instance.attackDistance = 3f;
        PlayerStats.instance.enemyHit = true;
        invisibility.SetActive(false);
        Destroy(gameObject);
    }
}
